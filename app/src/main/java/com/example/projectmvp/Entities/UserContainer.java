package com.example.projectmvp.Entities;

import java.util.List;

public class UserContainer {
    private List<User> results;

    public List<User> getResults() {
        return results;
    }

    public void setResults(List<User> results) {
        this.results = results;
    }
}
