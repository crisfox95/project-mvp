package com.example.projectmvp.list_user.Model;

import com.example.projectmvp.Entities.User;

import java.util.List;

public interface ListUserMvp {

    interface View{
        void listUser(List<User> listUser);
        void hideLoading();
        void visibleLoading();
        void hideSwipeRefresh();
        void toastSuccessfull();
    }

    interface Presenter{
        void pedirListUser(String gender);
        void respuestaListUser(List<User> listUser);
    }

    interface Model{
        void pedirARetrofitListUser(String gender);
    }

}
