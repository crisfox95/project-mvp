package com.example.projectmvp.list_user.Presenter;

import com.example.projectmvp.list_user.Model.ListUserActivityModel;
import com.example.projectmvp.list_user.Model.ListUserMvp;
import com.example.projectmvp.Entities.User;
import com.example.projectmvp.list_user.View.Activities.ListUserActivity;

import java.util.List;

public class ListUserActivityPresenter implements ListUserMvp.Presenter {

    private ListUserActivity view;
    private ListUserActivityModel model;

    public ListUserActivityPresenter(ListUserActivity view) {
        this.view = view;
        this.model = new ListUserActivityModel(this,view.getApplicationContext());
    }


    @Override
    public void pedirListUser(String gender) {
        view.visibleLoading();
        model.pedirARetrofitListUser(gender);
    }


    @Override
    public void respuestaListUser(List<User> listUser) {
            view.listUser(listUser);
            view.hideLoading();
            view.hideSwipeRefresh();
            view.toastSuccessfull();

    }

}
