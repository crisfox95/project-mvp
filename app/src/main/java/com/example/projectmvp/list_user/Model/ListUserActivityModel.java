package com.example.projectmvp.list_user.Model;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.projectmvp.Entities.Coordinates;
import com.example.projectmvp.Entities.Dob;
import com.example.projectmvp.Entities.Location;
import com.example.projectmvp.Entities.Name;
import com.example.projectmvp.Entities.Picture;
import com.example.projectmvp.Entities.User;
import com.example.projectmvp.Entities.UserContainer;
import com.example.projectmvp.Service.ServiceApi;
import com.example.projectmvp.Service.ServiceRetrofit;
import com.example.projectmvp.list_user.Presenter.ListUserActivityPresenter;
import com.example.projectmvp.list_user.View.Activities.ListUserActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListUserActivityModel implements ListUserMvp.Model {

    private ListUserActivityPresenter presenter;
    private Context context;

    public ListUserActivityModel(ListUserActivityPresenter presenter, Context context) {
        this.presenter = presenter;
        this.context = context;
    }

    @Override
    public void pedirARetrofitListUser(String gender) {

        final ServiceApi serviceApi = ServiceRetrofit.getRetrofit().create(ServiceApi.class);

        Call<UserContainer> userContainerCall = serviceApi.getListUsers(gender,50);

        userContainerCall.enqueue(new Callback<UserContainer>() {
            @Override
            public void onResponse(Call<UserContainer> call, Response<UserContainer> response) {
                if(internetAvailable()){

                    if (response.body() != null){
                        if (!response.body().getResults().isEmpty()){
                            presenter.respuestaListUser(response.body().getResults());
                        }
                    }
                }else {
                    presenter.respuestaListUser(metodohard());
                }
            }

            @Override
            public void onFailure(Call<UserContainer> call, Throwable t) {
                presenter.respuestaListUser(metodohard());
                Log.e("m","Falló retrofit",t);
            }
        });

    }

    private boolean internetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    private List<User> metodohard() {
        List<User> list = new ArrayList();
        User user = new User();
        user.setGender("male");
        Name name = new Name();
        name.setFirst("Cristian");
        name.setLast("Cancelo");
        user.setName(name);
        Dob dob = new Dob();
        dob.setAge("23");
        user.setDob(dob);
        user.setEmail("crisfox95@gmail.com");
        Location location = new Location();
        Coordinates coordinates = new Coordinates();
        coordinates.setLatitude("34.034");
        coordinates.setLongitude("-213333");
        location.setCoordinates(coordinates);
        user.setLocation(location);
        Picture picture = new Picture();
        picture.setMedium("https://scontent.faep8-2.fna.fbcdn.net/v/t1.0-9/26167450_10215316293329268_2853763122349593038_n.jpg?_nc_cat=108&_nc_oc=AQlEAuUGl_qxWXOeS0hC2JTBVpaSlOXqXFexEH6J6uTrzFMD-LraIsZ7ttI_xxzhyLA&_nc_ht=scontent.faep8-2.fna&oh=a45dfc86faabfcd658136c5645793a9a&oe=5DF6BB8F");
        picture.setLarge("https://scontent.faep8-2.fna.fbcdn.net/v/t1.0-9/26167450_10215316293329268_2853763122349593038_n.jpg?_nc_cat=108&_nc_oc=AQlEAuUGl_qxWXOeS0hC2JTBVpaSlOXqXFexEH6J6uTrzFMD-LraIsZ7ttI_xxzhyLA&_nc_ht=scontent.faep8-2.fna&oh=a45dfc86faabfcd658136c5645793a9a&oe=5DF6BB8F");
        user.setPicture(picture);

        list.add(user);
        return list;
    }



}
