package com.example.projectmvp.list_user.View.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.projectmvp.Entities.User;
import com.example.projectmvp.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdapterRecyclerViewListUser extends RecyclerView.Adapter<AdapterRecyclerViewListUser.ViewHolder> {

    private List<User> listUser;
    private List<User> listFilter;
    private List<User> listTemp;
    private Context context;
    private InterfaceClickItem interfaceClickItem;

    public AdapterRecyclerViewListUser(InterfaceClickItem interfaceClickItem) {
        this.interfaceClickItem = interfaceClickItem;
        this.listUser = new ArrayList<>();
        this.listTemp = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.cell_recyclerview_listuser,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = listUser.get(position);
        holder.textViewFirstUser.setText(user.getName().getFirst());
        holder.textViewLastUser.setText(user.getName().getLast());
        Glide.with(context).load(user.getPicture().getMedium()).into(holder.imageViewUser);
    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewFirstUser;
        private TextView textViewLastUser;
        private ImageView imageViewUser;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewFirstUser = itemView.findViewById(R.id.textViewFirstListUser);
            textViewLastUser = itemView.findViewById(R.id.textViewLastListUser);
            imageViewUser = itemView.findViewById(R.id.imageViewListUser);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    interfaceClickItem.clickItem(listUser.get(getAdapterPosition()));
                }
            });

        }
    }

    public void loadUsers(List<User> listUser){
        this.listUser.clear();
        this.listUser.addAll(listUser);
        this.listTemp.addAll(listUser);
        notifyDataSetChanged();
    }

    public void loadFilterText(String text){

        listFilter = new ArrayList<>();
        listFilter.addAll(listTemp);
        if (text.length() != 0) {
            listUser.clear();
            for (User user : listFilter) {
                if (user.getName().toString().toLowerCase(Locale.getDefault()).contains(text)) {
                    listUser.add(user);
                }
            }
        }else{
            listUser.clear();
            listUser.addAll(listTemp);
        }
        notifyDataSetChanged();
    }

    public interface InterfaceClickItem {
        void clickItem(User user);
    }
}
