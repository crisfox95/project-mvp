package com.example.projectmvp.list_user.View.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.projectmvp.Utils.Constantes;
import com.example.projectmvp.list_user.Model.ListUserMvp;
import com.example.projectmvp.list_user.Presenter.ListUserActivityPresenter;
import com.example.projectmvp.list_user.View.Adapter.AdapterRecyclerViewListUser;
import com.example.projectmvp.Entities.User;
import com.example.projectmvp.R;
import com.example.projectmvp.perfil.PerfilActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class ListUserActivity extends AppCompatActivity implements ListUserMvp.View, AdapterRecyclerViewListUser.InterfaceClickItem {

    private ListUserActivityPresenter presenter;
    private RecyclerView recyclerView;
    private AdapterRecyclerViewListUser adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private BottomNavigationView navigation;
    private Handler handler;
    private boolean backKeyPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backKeyPressed = false;
        handler = new Handler(Looper.getMainLooper());

        //Creo un nuevo presenter y le paso esta misma vista como parametro
        presenter = new ListUserActivityPresenter(this);
        initViews();
        initAdapter();
        //Le pido la lista al presenter
        presenter.pedirListUser("");

        refreshSwipe();

        setSupportActionBar(toolbar);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_user:
                    intentActivityPerfil();
                    return true;
            }
            return false;
        }
    };

    private void initAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        recyclerView.setHasFixedSize(true);
        adapter = new AdapterRecyclerViewListUser(this);
        recyclerView.setAdapter(adapter);
    }

    private void initViews() {
        recyclerView = findViewById(R.id.recyclerViewListUser);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayoutRecyclerView);
        toolbar = findViewById(R.id.toolbarRecyclerView);
        progressBar = findViewById(R.id.progressBarRecycler);
        navigation = findViewById(R.id.bottomNavigationViewListUser);
    }

    private void refreshSwipe(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.pedirListUser("");
            }
        });
    }

    @Override
    public void listUser(List<User> listUser) {
        adapter.loadUsers(listUser);
    }

    @Override
    public void hideLoading() {
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void visibleLoading() {
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSwipeRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void toastSuccessfull() {
        Toast.makeText(this, "La carga se ha realizado con éxito!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void clickItem(User user) {
        Intent intent = new Intent(this, DetailUserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constantes.FIRST_USER,user.getName().getFirst());
        bundle.putString(Constantes.LAST_USER,user.getName().getLast());
        bundle.putString(Constantes.IMAGE_USER,user.getPicture().getLarge());
        bundle.putString(Constantes.EMAIL_USER,user.getEmail());
        bundle.putString(Constantes.AGE_USER,user.getDob().getAge());
        bundle.putString(Constantes.LATITUDE_USER,user.getLocation().getCoordinates().getLatitude());
        bundle.putString(Constantes.LONGITUDE_USER,user.getLocation().getCoordinates().getLongitude());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);

        final MenuItem searchItem;
        final SearchView searchView;

        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_DEL) {
                    backKeyPressed = true;
                }else{
                    backKeyPressed = false;
                }
                Log.d("onKey",Boolean.toString(backKeyPressed));
                return false;

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                adapter.loadFilterText(s);

                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_filter:
                crearDialog();
                return true;
            case R.id.action_user:
                intentActivityPerfil();
                return true;

            default:return super.onOptionsItemSelected(item);
        }
    }

    public void intentActivityPerfil(){
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void crearDialog() {
        final ArrayList<String> listaDeGeneros = new ArrayList<>();
        listaDeGeneros.add("Male");
        listaDeGeneros.add("Female");
        listaDeGeneros.add("Ambos");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_selectable_list_item,listaDeGeneros);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Filtro");
        builder.setSingleChoiceItems(arrayAdapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (listaDeGeneros.get(i).equals("Ambos")){
                    presenter.pedirListUser("");
                }
                presenter.pedirListUser(listaDeGeneros.get(i).toLowerCase());
                dialogInterface.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


}
