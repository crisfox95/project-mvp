package com.example.projectmvp.list_user.View.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.projectmvp.R;
import com.example.projectmvp.Utils.Constantes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailUserActivity extends AppCompatActivity implements OnMapReadyCallback{

    //@BindView(R.id.imageViewUserDetail)
    private ImageView imageViewDetail;
    //@BindView(R.id.textViewNameCompleteDetail)
    private TextView textViewNameComplete;
    private TextView textViewEmailUser;
    private TextView textViewAgeUser;
    private GoogleMap mMap;
    private MapView mapView;
    private LatLng userLatLng;
    private String nombreCompleto;
    private static final int LOCATION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);
        //ButterKnife.bind(this);
        initViews();
        setViews();

        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }

    }

    private void initViews() {
        imageViewDetail = findViewById(R.id.imageViewUserDetail);
        textViewNameComplete = findViewById(R.id.textViewNameCompleteDetail);
        textViewEmailUser = findViewById(R.id.textViewEmailUserDetail);
        textViewAgeUser = findViewById(R.id.textViewAgeUserDetail);
        mapView = findViewById(R.id.map);
    }

    private void setViews() {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        assert bundle != null;
        String userFirst = bundle.getString(Constantes.FIRST_USER);
        String userLast = bundle.getString(Constantes.LAST_USER);
        String userImage = bundle.getString(Constantes.IMAGE_USER);
        String userEmail = bundle.getString(Constantes.EMAIL_USER);
        String userAge = bundle.getString(Constantes.AGE_USER);
        String userLatitude = bundle.getString(Constantes.LATITUDE_USER);
        String userLongitude = bundle.getString(Constantes.LONGITUDE_USER);
        double userLatitudeDouble = Double.valueOf(userLatitude);
        double userLongitudeDouble = Double.valueOf(userLongitude);

        nombreCompleto = userFirst + " " + userLast;

        userLatLng = new LatLng(userLatitudeDouble,userLongitudeDouble);

        Glide.with(this).load(userImage).into(imageViewDetail);
        textViewNameComplete.setText(nombreCompleto);
        textViewEmailUser.setText(userEmail);
        textViewAgeUser.setText(userAge);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.addMarker(new MarkerOptions().position(userLatLng).title(nombreCompleto).snippet("Ubicación actual"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(userLatLng));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            //Si tengo los permisos son aceptados, pongo mi ubicacion en el mapa, sino informo el error.
            if (permissions.length > 0 &&
                    permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            } else {
                Toast.makeText(this, this.getResources().getString(R.string.deniedPermits), Toast.LENGTH_LONG).show();
                Log.e("FragmentBussines","No se tiene permisos para obtener la ubicacion");
            }
        }
    }
}
