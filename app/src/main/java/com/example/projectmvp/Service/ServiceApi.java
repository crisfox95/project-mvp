package com.example.projectmvp.Service;

import com.example.projectmvp.Entities.UserContainer;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET("api/?")
    Call<UserContainer> getListUsers(@Query("gender") String gender, @Query("results") Integer cantidadDeUsers);


}
