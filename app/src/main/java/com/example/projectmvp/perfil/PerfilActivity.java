package com.example.projectmvp.perfil;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmvp.R;
import com.example.projectmvp.login.View.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class PerfilActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;
    private TextView textViewNameUserPerfil;
    private Button buttonLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        initViews();

        textViewNameUserPerfil.setText(currentUser.getEmail());
        setButtonLogout();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, LoginActivity.class);
        if (currentUser == null){
            startActivity(intent);
        }else {
            Toast.makeText(this, "Hay un usuario iniciado", Toast.LENGTH_SHORT).show();
        }
    }

    private void initViews() {
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        textViewNameUserPerfil = findViewById(R.id.textViewNameUserPerfil);
        buttonLogout = findViewById(R.id.buttonLogout);
    }

    private void setButtonLogout(){
        final Intent intent = new Intent(this,LoginActivity.class);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUser != null){
                    firebaseAuth.signOut();
                    startActivity(intent);
                }
            }
        });
    }
}
