package com.example.projectmvp.Utils;

public class Constantes {

    public static final String FIRST_USER = "FIRST_USER";
    public static final String LAST_USER = "LAST_USER";
    public static final String IMAGE_USER = "IMAGE_USER";
    public static final String EMAIL_USER = "EMAIL_USER";
    public static final String AGE_USER = "AGE_USER";
    public static final String POSITION_USER = "POSITION_USER";
    public static final String LATITUDE_USER = "LATITUDE_USER";
    public static final String LONGITUDE_USER = "LONGITUDE_USER";
}
