package com.example.projectmvp.login.Model;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.projectmvp.login.Presenter.LoginActivityPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivityModel implements LoginActivityMVP.model {

    private LoginActivityPresenter presenter;
    private FirebaseAuth mAuth;

    public LoginActivityModel(LoginActivityPresenter presenter) {
        this.presenter = presenter;
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public FirebaseUser pedirAFirebaseUser() {
        return mAuth.getCurrentUser();
    }

    @Override
    public void cargarAFirebaseNewUser(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("m","createUserWithEmail:Success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            presenter.respuestaUpdateUISuccessful(user);
                        }else{
                            Log.w("m", "createUserWithEmail:failure", task.getException());
                            presenter.respuestaUpdateUIFallo();
                        }

                    }
                });
    }

    @Override
    public void loginAFirebaseUser(final String email, final String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            Log.d("m", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            presenter.respuestaUpdateUISuccessful(user);
                        } else {
                            Log.w("m", "signInWithEmail:failure", task.getException());
                            //crear USER
                            presenter.cargarRegistro(email,password);
                        }


                    }
                });
    }
}
