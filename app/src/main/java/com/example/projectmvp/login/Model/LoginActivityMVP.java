package com.example.projectmvp.login.Model;

import com.google.firebase.auth.FirebaseUser;

public interface LoginActivityMVP {

    interface view{
        void hideProgressBar();
        void visibleProgressBar();
        void toastErrorExiste();
        void toastErrorEmptyUsernamePassword();
        void toastErrorValidationFalse();
        void updateUI(FirebaseUser firebaseUser);
    }

    interface presenter{
        FirebaseUser getCurrentUser();
        void respuestaUpdateUISuccessful(FirebaseUser firebaseUser);
        void respuestaUpdateUIFallo();
        void cargarRegistro(String email, String password);
        void clickButtonLogin(String email, String password, boolean validacionOkUsername, boolean validacionOkPassword);
    }

    interface model{
        FirebaseUser pedirAFirebaseUser();
        void cargarAFirebaseNewUser(String email, String password);
        void loginAFirebaseUser(String email, String password);
    }
}
