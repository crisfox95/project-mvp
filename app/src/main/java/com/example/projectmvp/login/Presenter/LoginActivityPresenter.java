package com.example.projectmvp.login.Presenter;

import com.example.projectmvp.login.Model.LoginActivityMVP;
import com.example.projectmvp.login.Model.LoginActivityModel;
import com.example.projectmvp.login.View.LoginActivity;
import com.google.firebase.auth.FirebaseUser;


public class LoginActivityPresenter implements LoginActivityMVP.presenter {

    private LoginActivity view;
    private LoginActivityModel model;

    public LoginActivityPresenter(LoginActivity view) {
        this.view = view;
        model = new LoginActivityModel(this);
    }

    @Override
    public FirebaseUser getCurrentUser() {
        return model.pedirAFirebaseUser();
    }


    @Override
    public void respuestaUpdateUISuccessful(FirebaseUser firebaseUser) {
        view.updateUI(firebaseUser);
        view.hideProgressBar();
    }

    @Override
    public void respuestaUpdateUIFallo() {
        view.toastErrorExiste();
        view.hideProgressBar();
    }

    @Override
    public void cargarRegistro(String email, String password) {
        model.cargarAFirebaseNewUser(email,password);
    }

    @Override
    public void clickButtonLogin(String email, String password, boolean validacionOkUsername, boolean validacionOkPassword) {
        if(email.equals("") || password.equals("")){
            view.toastErrorEmptyUsernamePassword();
        }else {
            if (validacionOkUsername && validacionOkPassword){
                model.loginAFirebaseUser(email,password);
                view.visibleProgressBar();
            }else {
                view.toastErrorValidationFalse();
            }
        }

    }

}
