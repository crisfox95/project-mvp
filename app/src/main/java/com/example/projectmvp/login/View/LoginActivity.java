package com.example.projectmvp.login.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.projectmvp.R;
import com.example.projectmvp.list_user.View.Activities.ListUserActivity;
import com.example.projectmvp.login.Model.LoginActivityMVP;
import com.example.projectmvp.login.Presenter.LoginActivityPresenter;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.view {

    private LoginActivityPresenter presenter;
    private Button buttonLogin;
    private EditText editTextUsername;
    private EditText editTextPassword;
    private String email;
    private String password;
    private TextInputLayout textInputLayoutUsername;
    private TextInputLayout textInputLayoutPassword;
    private boolean validacionOkPassword;
    private boolean validacionOkUsername;
    private ProgressBar progressBar;
    private ConstraintLayout constraintLayout;

    @Override
    protected void onStart() {
        super.onStart();
        updateUI(presenter.getCurrentUser());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        presenter = new LoginActivityPresenter(this);

        initViews();
        setButtonLoginStart();
        setEditTextUsernameValidation();
        setEditTextPasswordValidation();
    }

    private void initViews() {
        buttonLogin = findViewById(R.id.buttonLogin);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        textInputLayoutUsername = findViewById(R.id.textInputEditTextUsername);
        textInputLayoutPassword = findViewById(R.id.textInputEditTextPassword);
        progressBar = findViewById(R.id.progressBarLoadingLogin);
        constraintLayout = findViewById(R.id.constraintLayout);
    }

    private void setEditTextPasswordValidation() {
        textInputLayoutUsername.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 0){
                    if (!isEmailValid(charSequence)) {
                        textInputLayoutUsername.setError(getString(R.string.required_email));
                        textInputLayoutUsername.setErrorEnabled(true);
                        validacionOkPassword = false;
                    } else {
                        textInputLayoutUsername.setErrorEnabled(false);
                        validacionOkUsername = true;
                    }
                }else{
                    textInputLayoutUsername.setErrorEnabled(false);
                    validacionOkPassword = false;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setEditTextUsernameValidation() {
        textInputLayoutPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(charSequence.toString().length() == 0){
                        textInputLayoutPassword.setErrorEnabled(false);
                        validacionOkPassword = false;
                    }else {
                        if(charSequence.toString().length()<8){
                            textInputLayoutPassword.setError("Ingrese una contraseña que contengan al menos 8 caracteres");
                            textInputLayoutPassword.setErrorEnabled(true);
                            validacionOkPassword = false;
                        }else{
                            if (!isValidPassword(charSequence.toString())){
                                textInputLayoutPassword.setError("Ingrese una contraseña que contenga una Mayúscula, un caracter especial y al menos un número. Por ejemplo 'Hola@1234'");
                                textInputLayoutPassword.setErrorEnabled(true);
                                validacionOkPassword = false;
                            }else{
                                textInputLayoutPassword.setErrorEnabled(false);
                                validacionOkPassword = true;
                            }
                        }

                    }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidPassword(CharSequence password) {
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password.toString());
        return matcher.matches();
    }

    private void setButtonLoginStart() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();
                presenter.clickButtonLogin(email,password,validacionOkUsername,validacionOkPassword);
            }
        });
    }

    @Override
    public void updateUI(FirebaseUser currentUser) {
        Intent intent = new Intent(this, ListUserActivity.class);
        if (currentUser != null){
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }else {
            hideProgressBar();
        }
    }

    @Override
    public void hideProgressBar(){
        constraintLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void visibleProgressBar(){
        constraintLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void setToast(Integer mensaje){
        Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastErrorExiste() {
        setToast(R.string.existe_user);
    }

    @Override
    public void toastErrorEmptyUsernamePassword() {
        setToast(R.string.email_password_empty);
    }

    @Override
    public void toastErrorValidationFalse() {
        setToast(R.string.validacion_false);
    }



}
